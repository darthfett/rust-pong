#![allow(dead_code)]
#![allow(unused_variables)]

extern crate piston_window;

mod ball_entity;
mod engine;
mod paddle_entity;
mod pong;


fn main() -> Result<(), std::io::Error> {
    let mut pong_game: pong::Pong = pong::Pong::new(640, 480);
    pong_game.run();

    Ok(())
}
