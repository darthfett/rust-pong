use piston_window;
use piston_window::types::Matrix2d;
use piston_window::G2d;

use engine::drawable::Drawable;
use engine::updatable::Updatable;

pub struct Paddle {
    pub x: f64,
    pub y: f64,
    pub vy: f64,
    pub width: f64,
    pub height: f64,
}

impl Drawable for Paddle {
    fn draw(&self, transform: Matrix2d, graphics: &mut G2d) {
        // Draw paddle
        piston_window::rectangle(
            [1.0, 1.0, 1.0, 1.0], // white
            [
                self.x - self.width / 2.,
                self.y - self.height / 2.,
                self.width,
                self.height,
            ],
            transform,
            graphics
        );
    }
}

impl Updatable for Paddle {
    fn update(&mut self, dt: f64) {
        self.y = self.y + (self.vy * dt);
    }
}