use piston_window::types::Vec2d;

pub struct Circle {
    pub center: Vec2d,
    pub radius: f64,
}

pub struct Rectangle {
    pub topleft: Vec2d,
    pub size: Vec2d,
}

pub enum Collidable {
    Circle(Circle),
    Rectangle(Rectangle),
}

pub trait Collision {
    fn collides_with(&self, collidable: &Collidable) -> bool;
}

impl Collision for Rectangle {
    fn collides_with(&self, collidable: &Collidable) -> bool {
        match collidable {
            Collidable::Circle(circle) => {
                true
            }
            Collidable::Rectangle(rectangle) => {
                !(
                    (self.topleft[0] + self.size[0] < rectangle.topleft[0]) ||
                    (self.topleft[0] > rectangle.topleft[0] + rectangle.size[0]) ||
                    (self.topleft[1] > rectangle.topleft[1] + rectangle.size[1]) ||
                    (self.topleft[1] + self.size[1] < rectangle.topleft[1])
                )
            }
        }
    }
}

