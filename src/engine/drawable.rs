use piston_window::types::Matrix2d;
use piston_window::G2d;

pub trait Drawable {
    fn draw(&self, transform: Matrix2d, graphics: &mut G2d);
}