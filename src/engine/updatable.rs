pub trait Updatable {
    fn update(&mut self, dt: f64);
}