use piston_window;
use piston_window::types::Matrix2d;
use piston_window::G2d;

use engine::drawable::Drawable;
use engine::updatable::Updatable;

pub const DIAMETER: f64 = 5.0;

pub struct Ball {
    pub x: f64,
    pub y: f64,
    pub vx: f64,
    pub vy: f64,
}

impl Drawable for Ball {
    fn draw(&self, transform: Matrix2d, graphics: &mut G2d) {
        // Draw Ball
        piston_window::rectangle(
            [1.0, 1.0, 1.0, 1.0], // white
            [
                self.x - DIAMETER / 2.,
                self.y - DIAMETER / 2.,
                DIAMETER,
                DIAMETER,
            ],
            transform,
            graphics
        );
    }
}

impl Updatable for Ball {
    fn update(&mut self, dt: f64) {
        self.x = self.x + (self.vx * dt);
        self.y = self.y + (self.vy * dt);
    }
}