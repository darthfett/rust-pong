extern crate piston_window;
use piston_window::{Button, ButtonState, Event, Input, Key, Loop, PistonWindow, RenderArgs, UpdateArgs, WindowSettings};

use super::ball_entity;
use super::ball_entity::Ball;
use super::engine::drawable::Drawable;
use engine::updatable::Updatable;
use super::paddle_entity::Paddle;

#[derive(Default)]
pub struct Pong {
    window_width: u32,
    window_height: u32,
    balls: Vec<Ball>,
    paddles: Vec<Paddle>,
    up_pressed: bool,
    down_pressed: bool,
}

impl Pong {

    fn on_key_press(&mut self, key: Key) {
        match key {
            Key::W => {
                if !self.up_pressed {
                    self.up_pressed = true;
                    for mut paddle in &mut self.paddles {
                        paddle.vy = paddle.vy - (self.window_height as f64 / 1.6).round();
                    }
                }
            }
            Key::S => {
                if !self.down_pressed {
                    self.down_pressed = true;
                    for mut paddle in &mut self.paddles {
                        paddle.vy = paddle.vy + (self.window_height as f64 / 1.6).round();
                    }
                }
            }
            _ => {}
        }
    }

    fn on_key_release(&mut self, key: Key) {
        match key {
            Key::W => {
                if self.up_pressed {
                    self.up_pressed = false;
                    for mut paddle in &mut self.paddles {
                        paddle.vy = paddle.vy + (self.window_height as f64 / 1.6).round();
                    }
                }
            }
            Key::S => {
                if self.down_pressed {
                    self.down_pressed = false;
                    for mut paddle in &mut self.paddles {
                        paddle.vy = paddle.vy - (self.window_height as f64 / 1.6).round();
                    }
                }
            }
            _ => {}
        }
    }

    fn on_input(&mut self, input: Input) {
        match input {
            Input::Button(btn) => {
                match btn.state {
                    ButtonState::Press => {
                        match btn.button {
                            Button::Keyboard(key) => {
                                self.on_key_press(key);
                            }
                            _ => {}
                        }
                    }
                    ButtonState::Release => {
                        match btn.button {
                            Button::Keyboard(key) => {
                                self.on_key_release(key);
                            }
                            _ => {}
                        }
                    }
                }
            }
            _ => {}
        }
    }

    fn on_update(&mut self, window: &mut PistonWindow, update_args: UpdateArgs) {
        for mut ball in &mut self.balls {
            ball.update(update_args.dt);

            let min_x = ball.x - ball_entity::DIAMETER / 2.;
            let max_x = ball.x + ball_entity::DIAMETER / 2.;
            let min_y = ball.y - ball_entity::DIAMETER / 2.;
            let max_y = ball.y + ball_entity::DIAMETER / 2.;
            if max_x > self.window_width as f64 {
                ball.x = ball.x + (self.window_width as f64 - max_x);
                ball.vx = -ball.vx;
            }
            if min_x < 0. {
                ball.x = ball.x - min_x;
                ball.vx = -ball.vx;
            }
            if max_y > self.window_height as f64 {
                ball.y = ball.y + (self.window_height as f64 - max_y);
                ball.vy = -ball.vy;
            }
            if min_y < 0. {
                ball.y = ball.y - min_y;
                ball.vy = -ball.vy;
            }
        }

        for mut paddle in &mut self.paddles {
            paddle.update(update_args.dt);

            let min_y = paddle.y as f64 - paddle.height as f64 / 2.;
            let max_y = paddle.y as f64 + paddle.height as f64 / 2.;
            if max_y > self.window_height as f64 {
                paddle.y = paddle.y + (self.window_height as f64 - max_y);
            }
            if min_y < 0. {
                paddle.y = paddle.y - min_y;
            }
        }
    }

    fn on_draw(&mut self, window: &mut PistonWindow, event: Event, render_args: RenderArgs) {
        window.draw_2d(&event, |context, graphics| {
            piston_window::clear([0.0, 0.0, 0.0, 1.0], graphics);
            for ball in &self.balls {
                ball.draw(context.transform, graphics);
            }
            for paddle in &self.paddles {
                paddle.draw(context.transform, graphics);
            }

        });
    }

    pub fn run(&mut self) {
        // Create a Piston window.
        let mut window: PistonWindow = WindowSettings::new(
            "pong",
            [self.window_width, self.window_height]
        )
        .exit_on_esc(true)
        .build()
        .unwrap();

        while let Some(event) = window.next() {
            match event {
                Event::Loop(loop_event) => {
                    match loop_event {
                        Loop::Update(update_args) => {
                            self.on_update(&mut window, update_args);
                        }
                        Loop::Render(render_args) => {
                            self.on_draw(&mut window, event, render_args);
                        }
                        _ => {}
                    }

                }
                Event::Input(input) => {
                    self.on_input(input);
                }
                _ => {}
            }
        }
    }

    pub fn new(width: u32, height: u32) -> Pong {
        let w = width as f64;
        let h = height as f64;
        let balls = vec![
            Ball {
                x: w / 3.,
                y: h / 2.,
                vx: w / 4.,
                vy: h / 4.,
            }
        ];
        let paddles = vec![
            Paddle {
                x: w / 8.,
                y: h / 2.,
                vy: 0.,
                width: w / 96.,
                height: w / 24.,
            },
            Paddle {
                x: w * 7. / 8.,
                y: h / 2.,
                vy: 0.,
                width: w / 96.,
                height: w / 24.,
            }
        ];

        Pong {
            window_width: width,
            window_height: height,
            balls: balls,
            paddles: paddles,
            ..Default::default()
        }
    }
}